package AddressBookCustomer.com.jaimeem.models;

import java.io.Serializable;

/**
 * Created by Maximiliano on 3/31/2016 AD.
 */
public class Customers implements Serializable {

    String id;
    String customer_name;
    String gender;
    String status;

    @Override
    public String toString() {
        return "Customers{" +
                "id='" + id + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", gender='" + gender + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
