package AddressBookCustomer.com.jaimeem.ui;

import AddressBookCustomer.com.jaimeem.models.Customers;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Maximiliano on 3/31/2016 AD.
 */
public class CustomerMainLayout extends VerticalLayout {

    private ArrayList<Customers> instanceCustomer = new ArrayList<>();

    public CustomerMainLayout() {

        MenuBar menuBar = new MenuBar();
        menuBar.addItem("Files",null);
        menuBar.addItem("Tools",null);
        menuBar.addItem("Edit",null);
        menuBar.addItem("Help",null);
        menuBar.setSizeFull();

        VerticalLayout mainLayout = new VerticalLayout();
        VerticalLayout sideLayout = new VerticalLayout();

        //MainLayout
        HorizontalLayout customerSection = new HorizontalLayout();
        Table customerList = customListTable();

        Component searchComponent = customerSearchComponent();
        Component overViewComponent = customerOverviewComponent();
        customerSection.addComponents(searchComponent,overViewComponent);
        customerSection.setExpandRatio(searchComponent,4);
        customerSection.setExpandRatio(overViewComponent,6);


        customerSection.setSizeFull();
        customerSection.setStyleName("section-bottom");
        customerList.setSizeFull();

        mainLayout.setSizeFull();
        mainLayout.setMargin(true);
        mainLayout.addComponents(customerSection,customerList);
        mainLayout.setExpandRatio(customerSection,4);
        mainLayout.setExpandRatio(customerList,6);
        //MainLayout

        Component newButton = sectionNewButton();
        Component bactchOperation = sectionBatch();
        Component summary = sectionSummary();
        Component categories = sectionCategories();

        categories.setHeight("85%");

        sideLayout.setSizeFull();
        sideLayout.setStyleName("myMargin");
        Button newCategory = new Button("New Category");
        newCategory.setId("new-catagory");
        sideLayout.addComponents(newButton,bactchOperation,summary,newCategory,categories);
        sideLayout.setExpandRatio(newButton,1);
        sideLayout.setExpandRatio(bactchOperation,1);
        sideLayout.setExpandRatio(summary,1);
        sideLayout.setExpandRatio(newCategory,0);
        sideLayout.setExpandRatio(categories,3);
        sideLayout.setComponentAlignment(newCategory,Alignment.MIDDLE_RIGHT);


        HorizontalLayout rootLayout = new HorizontalLayout();
        rootLayout.addComponents(mainLayout,sideLayout);
        rootLayout.setSizeFull();
        rootLayout.setExpandRatio(mainLayout,8);
        rootLayout.setExpandRatio(sideLayout,2);

        addComponents(menuBar,rootLayout);
        setExpandRatio(menuBar,0.5f);
        setExpandRatio(rootLayout,10);

    }

    private Component sectionCategories() {
//        VerticalLayout categoriesLayout = new VerticalLayout();
//        Button newCatogoriesbtn = new Button();
//        newCatogoriesbtn.setCaption("New Category");
        Table categoryList = new Table();
        categoryList.setSizeFull();
        return categoryList;
    }

    private Component sectionSummary() {
        VerticalLayout summaryLayout = new VerticalLayout();
        Label summaryCustomer = new Label("" +
                "Total : 1<br>" +
                "Active : 1<br>" +
                "Inactive : 1<br>" +
                "Terminated : 1<br>");
        summaryCustomer.setContentMode(ContentMode.HTML);
        summaryCustomer.setId("summary-customer");
        summaryLayout.addComponent(summaryCustomer);
        summaryLayout.setSizeFull();
        return summaryLayout;
    }

    private Component sectionBatch() {
        VerticalLayout batchLayout = new VerticalLayout();
        ComboBox batchStatus = new ComboBox();
        Button runBatchbtn = new Button();
        runBatchbtn.setCaption("Run Batch");
        batchStatus.setWidth("100%");
        batchStatus.setHeight("80%");
        batchStatus.setInputPrompt("Print Checked Statement");
        batchLayout.addComponents(batchStatus,runBatchbtn);
//        batchLayout.setSizeFull();
        return batchLayout;
    }

    private Component sectionNewButton() {
        VerticalLayout newButtonLayout = new VerticalLayout();
        Button newCustomerbtn = new Button();
        newCustomerbtn.setCaption("New Customer");
        newCustomerbtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Notification.show("Click me!!");
            }
        });
        newButtonLayout.addComponent(newCustomerbtn);
//        newButtonLayout.setHeight("15%");
        newButtonLayout.setComponentAlignment(newCustomerbtn,Alignment.MIDDLE_CENTER);
        return newButtonLayout;
    }

    public Component customerSearchComponent(){
        VerticalLayout csComponent = new VerticalLayout();
//        csComponent.setSizeFull();
//        csComponent.setMargin(true);
//        csComponent.setSpacing(true);
        csComponent.setSizeFull();
        Label textSearch = new Label("");
        textSearch.setCaption("Customer Search");
        ComboBox ctmNameCB = new ComboBox();
        ComboBox ctmContainsCB = new ComboBox();
        TextField textString = new TextField();

        ctmNameCB.setWidth("100%");
        ctmContainsCB.setWidth("100%");
        textString.setWidth("100%");

        ctmNameCB.setInputPrompt("Customer Name");
        ctmContainsCB.setInputPrompt("Contains");
        textString.setInputPrompt("typing");

        ctmNameCB.setHeight("80%");
        ctmContainsCB.setHeight("80%");
        textString.setHeight("80%");

        HorizontalLayout h1 = new HorizontalLayout();
        ComboBox amount = new ComboBox();
        amount.setHeight("100%");
        amount.setInputPrompt("amount records");
        h1.addComponents(amount,new Label("Result at a time"));
        h1.setWidth("100%");
        HorizontalLayout h2 = new HorizontalLayout();
        h2.addComponents(new CheckBox("Active"), new CheckBox("Inactive"), new CheckBox("Term"));
        h2.setWidth("100%");
        HorizontalLayout h3 = new HorizontalLayout();
        h3.addComponents(new Button("Search"), new Button("Reset"));
        h3.setWidth("100%");


        csComponent.addComponents(textSearch,ctmNameCB,ctmContainsCB,textString,h1,h2,h3);
        csComponent.setComponentAlignment(textString,Alignment.TOP_CENTER);
        csComponent.setComponentAlignment(ctmNameCB,Alignment.MIDDLE_CENTER);
        csComponent.setComponentAlignment(ctmContainsCB,Alignment.MIDDLE_CENTER);
        csComponent.setComponentAlignment(textString,Alignment.MIDDLE_CENTER);
        csComponent.setComponentAlignment(h1,Alignment.MIDDLE_CENTER);
        csComponent.setComponentAlignment(h2,Alignment.MIDDLE_CENTER);
        csComponent.setComponentAlignment(h3,Alignment.MIDDLE_CENTER);
        return csComponent;
    }

    public Component customerOverviewComponent(){
        HorizontalLayout coComponent = new HorizontalLayout();
        coComponent.setWidth("90%");
        coComponent.setStyleName("border-overview");

        VerticalLayout coLeft = new VerticalLayout();
        VerticalLayout coRight = new VerticalLayout();

        coLeft.setSizeFull();
        coRight.setSizeFull();

        Label customerProfile = new Label("[C1000]<br>" +
                "Active<br>" +
                "Demo Customer 1<br>" +
                "112 Street East<br>" +
                "Anytown KY 41100<br>" +
                "P : 090-989-8989<br>" +
                "F : 02-678-8888");
        customerProfile.setContentMode(ContentMode.HTML);
        customerProfile.setCaption("Customer Quick View");
        customerProfile.setStyleName("label-padding");
        coLeft.addComponent(customerProfile);

        Label customerBalance = new Label(
                        "Customer Balance : $31.31<br>" +
                        "Past Due Balance : $10.15<br>"
        );
        customerBalance.setContentMode(ContentMode.HTML);

        Label textSum = new Label(
                        "1-30 : $0.00           31-60 : $0.00<br>" +
                        "61-90 : $10.15         90+ : $0.00"
        );
        textSum.setContentMode(ContentMode.HTML);
        textSum.setStyleName("label-padding");
        customerBalance.setStyleName("label-padding");
        coRight.addComponents(customerBalance,textSum);

        coComponent.addComponents(coLeft,coRight);

        return coComponent;
    }

    public Table customListTable(){
        Table customerTable = new Table();

        String json = "[{\n" +
                "  \"id\": 1,\n" +
                "  \"customer_name\": \"Gérald\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": false\n" +
                "}, {\n" +
                "  \"id\": 2,\n" +
                "  \"customer_name\": \"Tú\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": false\n" +
                "}, {\n" +
                "  \"id\": 3,\n" +
                "  \"customer_name\": \"Judicaël\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 4,\n" +
                "  \"customer_name\": \"Marlène\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 5,\n" +
                "  \"customer_name\": \"Clémentine\",\n" +
                "  \"gender\": \"Male\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 6,\n" +
                "  \"customer_name\": \"Bérénice\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": false\n" +
                "}, {\n" +
                "  \"id\": 7,\n" +
                "  \"customer_name\": \"Séverine\",\n" +
                "  \"gender\": \"Male\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 8,\n" +
                "  \"customer_name\": \"Mélodie\",\n" +
                "  \"gender\": \"Female\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 9,\n" +
                "  \"customer_name\": \"Maëline\",\n" +
                "  \"gender\": \"Male\",\n" +
                "  \"status\": true\n" +
                "}, {\n" +
                "  \"id\": 10,\n" +
                "  \"customer_name\": \"Magdalène\",\n" +
                "  \"gender\": \"Male\",\n" +
                "  \"status\": true\n" +
                "}]";

        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            Customers customers = new Customers();
            customers.setId(jsonObject.get("id").toString());
            customers.setCustomer_name(jsonObject.get("customer_name").toString());
            customers.setGender(jsonObject.get("gender").toString());
            customers.setStatus(jsonObject.get("status").toString());
            instanceCustomer.add(customers);
        }

        BeanItemContainer<Customers> bic = new BeanItemContainer<Customers>(Customers.class);
        for (Customers customers : instanceCustomer){
            bic.addItem(customers);
        }

        customerTable.setContainerDataSource(bic);
        customerTable.setVisibleColumns("id","customer_name","gender","status");
        customerTable.setColumnHeader("customer_name","Customer Name");
        customerTable.setColumnHeader("id","ID");
        customerTable.setColumnHeader("gender","Gender");
        customerTable.setColumnHeader("status","Status");

        customerTable.setSelectable(true);
        customerTable.addItemClickListener( e -> {
            Notification.show(e.getItemId().toString());
        });
        return customerTable;
    }

}
